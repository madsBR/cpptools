#pragma once
#include <algorithm>
#include <iostream>
#include <utility>
#include <type_traits>
#include <chrono>
#include <map>
#include <string>


inline int  pair_max(std::pair<int, int> p) { return std::max(p.first, p.second); };


using namespace std;
using Sep_Space = integral_constant<char, ' '>;
using Sep_Comma = integral_constant<char, ','>;

template<typename T=void>
std::void_t<T> println() { //ALWAYS end of recursion declared first
    cout << endl;
};


template<typename Sep_Class = Sep_Space, typename  First, typename... Rest>
void println(const First& first, const Rest&... rest) {
    cout << first << Sep_Class::value;
    println(rest...);
};



class Test {
private:
    template<bool b, typename  First, typename... Rest>
    static constexpr typename std::enable_if<b>::type print_it(const First& first, const Rest&... rest) {
        println<Sep_Space, First, Rest...>(first, rest...);
    };
    template<bool b, typename  First, typename... Rest>
    static constexpr typename std::enable_if<!b>::type  print_it(...) {
        //Null
    };
    using Clock = std::chrono::steady_clock;
    using Duration = Clock::duration;
    using Time_Point = Clock::time_point;

    template<bool b> static typename std::enable_if<b>::type add_time_stamp_pre(std::string str) {
        time_stamps[str] = Clock::now();
    };
    template<bool b> static typename std::enable_if<!b>::type add_time_stamp_pre(...) {};

    template<bool b> static typename std::enable_if<b>::type print_duration_pre(std::string before, std::string after) {
        print_test(std::chrono::duration_cast<std::chrono::milliseconds>(time_stamps[after] - time_stamps[before]).count());
    };
    template<bool b> static typename std::enable_if<!b>::type print_duration_pre(std::string before, std::string after) {};


public:
    constexpr static bool ctest{ true };
    constexpr static bool ctest_verbosity{ false };
    static_assert(ctest || !(ctest || ctest_verbosity), "either turn ctest on, or turn both off");
    static std::map<std::string, Time_Point> time_stamps;

    template<typename  First, typename... Rest>
    void static constexpr print_test(const First& first, const Rest&... rest) {
        print_it<ctest, First, Rest...>(first, rest...);
    };
    template<typename T = void> static std::void_t<T> add_time_stamp(std::string str) { add_time_stamp_pre<ctest>(str); };
    template<typename T = void> static std::void_t<T> print_duration(std::string bef, std::string aft) { print_duration_pre<ctest>(bef, aft); };
};

