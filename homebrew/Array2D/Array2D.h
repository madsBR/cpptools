#pragma once
#include <vector>
#include "homebrew/Array2D/Iterator1.h"
#include <stdexcept>
#include <algorithm>
template <typename elem_t = int>
class Array2D
{
protected:
// STL like
// supports iterator
//	based on vector container
	int size1_;
	int size2_;
	int size_;

public:
	std::vector<elem_t> data;
	using iterator = typename std::vector<elem_t>::iterator;
	using iterator1 = Iterator1<elem_t>;
	using iterator2 = iterator;

	Array2D(int rows, int cols,elem_t fill_value) : size1_{ rows }, size2_{ cols }, size_{ size1_ * size2_ }, data(rows * cols,fill_value) {};
	Array2D(int rows, int cols) : size1_{ rows }, size2_{ cols }, size_{ size1_ * size2_ }, data(rows* cols) {}


	elem_t& at(int i, int j) {
		if (i < size1) {
			if (j < size2) {
				(*this)(data_index(i, j));
			}
			else {
				throw std::range_error("col index");
			}
		}
		else {
			throw std::range_error("row index");
		}
	};

	inline int data_index(int row, int col) const { return row * size2_ + col; };
	inline elem_t& operator()(int i, int j) { return data[data_index(i, j)]; };
	inline elem_t& operator()(iterator1 iter, int j) { return *(iter.iter_vec + j); };
	inline int size1() const { return size1_; };
	inline int size2() const { return size2_; };
	inline int size() const { return size_; };
	inline iterator begin() { return data.begin(); }
	inline iterator end() { return data.end(); }
	inline iterator1 begin1() { return iterator1(data.begin(),size2_); };
	inline iterator1 end1() { return iterator1(data.begin()+size_,size2_); };
	inline iterator2 begin2(iterator1& iter) {return iterator2(iter.iter_vec);	};
	inline iterator2 end2(iterator1& iter) { return iterator2(iter.iter_vec + size2_); };
	inline iterator2 begin2(int row_nr) { return data.begin() + row_nr * size2_; };
	inline iterator2 end2(int row_nr) { return data.begin() + (row_nr+1) * size2_; };
	inline void insert(iterator iter, const elem_t type) { data.insert(iter, type); };
	inline void clear() { data.clear(); };
	inline void erase_row(iterator1 from, iterator1 to) {
		size1_ -= (to - from);
		size_ = size1_ * size2_;
		data.erase(from.iter_vec,to.iter_vec);
	}
	inline Array2D erase_row_copy(iterator1 from,iterator1 to){
		Array2D result(size1_ - (to - from), size2_);
		std::copy<iterator, iterator>(this->begin(), from.iter_vec, result.begin());
		std::copy<iterator, iterator>(to.iter_vec, this->end(), result.begin() + (to.iter_vec - this->begin()));
	}

	void print_teaser(int row_print_nr = 3, int col_print_nr = 3) {
		if (size1_ < row_print_nr) row_print_nr = size1_;
		if (size2_ < col_print_nr) col_print_nr = size2_;
		for (int row = 0; row < row_print_nr; row++) {
			for (int col = 0; col < col_print_nr; col++) {
				std::cout << (*this)(row, col) << " ";
			}
			std::cout <<"..."<<std::endl;
		}
		std::cout << "..." << std::endl;
	}
	void print() {
		for (int row = 0; row < size1_; row++) {
			for (int col = 0; col < size2_; col++) {
				std::cout << (*this)(row, col) << " ";
			}
			std::cout << std::endl;
		}
		std::cout << "." << std::endl;
	}

	virtual ~Array2D(){}

};


