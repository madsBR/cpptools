#pragma once
#include <cstddef>
#include <vector>
template<typename Type>
class Iterator1
{
public:
    using vec_t =typename  std::vector<Type>;
    using difference_type =typename std::ptrdiff_t;
    using Vec_iter =typename vec_t::iterator;
    Iterator1(const Vec_iter  rhs, int size2) : iter_vec{ rhs }, size2{ size2 } {
    } //from vec_iter
    Iterator1(const Iterator1& rhs) : iter_vec(rhs.iter_vec), size2 {rhs.size2} {} //copy
    /* inline Iterator11& operator=(Vec_iter rhs) {iter_vec = rhs; return *this;} */
    /* inline Iterator11& operator=(const Iterator11 &rhs) {iter_vec = rhs.iter_vec; return *this;} */
    inline Iterator1& operator+=(difference_type rhs) { iter_vec += size2 * rhs ; return *this; }
    inline Iterator1& operator-=(difference_type rhs) { iter_vec -= size2 * rhs; return *this; }
    inline Type& operator*() const { return *iter_vec; }
    inline Vec_iter operator->() const { return iter_vec; }
    inline Type& operator[](difference_type rhs) const { return iter_vec[rhs]; }
    inline Iterator1& operator++() { iter_vec += size2; return *this; }
    inline Iterator1& operator--() { iter_vec -= size2; return *this; }
    inline Iterator1 operator++(int) { Iterator1 tmp(*this); iter_vec += size2; return tmp; }
    inline Iterator1 operator--(int) { Iterator1 tmp(*this); iter_vec -= size2; return tmp; }
//Didnt know how to implement difference-type with division:

    /* inline Iterator1 operator+(const Iterator1& rhs) {return Iterator1(iter_vec+rhs.ptr);} */
    inline difference_type operator-(const Iterator1& rhs) const { return (iter_vec - rhs.iter_vec)/size2; }
    inline Iterator1 operator+(difference_type rhs) const { return Iterator1(iter_vec + rhs * size2, size2,0); }
//    inline Iterator1 operator-(difference_type rhs) const { return Iterator1(iter_vec - rhs / size2); }
//    friend inline Iterator1 operator+(difference_type lhs, const Iterator1& rhs) { return Iterator1(lhs / size2 + rhs.iter_vec); }
//    friend inline Iterator1 operator-(difference_type lhs, const Iterator1& rhs) { return Iterator1(lhs / size2 - rhs.iter_vec); }

    inline bool operator==(const Iterator1& rhs) const { return iter_vec == rhs.iter_vec; }
    inline bool operator!=(const Iterator1& rhs) const { return iter_vec != rhs.iter_vec; }
    inline bool operator>(const Iterator1& rhs) const { return iter_vec > rhs.iter_vec; }
    inline bool operator<(const Iterator1& rhs) const { return iter_vec < rhs.iter_vec; }
    inline bool operator>=(const Iterator1& rhs) const { return iter_vec >= rhs.iter_vec; }
    inline bool operator<=(const Iterator1& rhs) const { return iter_vec <= rhs.iter_vec; }

    inline Iterator1 offset(int col) {
        return Iterator1(iter_vec+col, size2);
    }
    Vec_iter iter_vec;
private:
    int size2;
//    long long sz = sizeof(Type);

};
