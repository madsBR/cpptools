#pragma once
#include <iostream>
struct Runnable {
protected:
	virtual void job_to_run() = 0; //abstract member function or "pure virtual member function"
public:
	bool done = false;
	void run() {
		job_to_run();
		done = true;
	}
	virtual ~Runnable() {};

private:
};

