#pragma once
#include <thread>
#include "Conc_Jobs_Container.h"
#include "Runnable.h"
#include <vector>
#include <atomic>
class ThreadPool {
	int worker_nr;
	Conc_Jobs_Container& job_container;

	void work_routine() {

		while (!job_container.is_completed.load()) {
			job_container.dispatch_next_job();
		}
	}

public:
	std::vector<std::thread> threads{};
	ThreadPool(Conc_Jobs_Container& job_container_, int worker_nr_) : worker_nr{ worker_nr_ }, job_container{ job_container_ } {
		threads.reserve(worker_nr);
	};


	void start() {
		for (int i = 0; i < worker_nr; i++) {
			threads.emplace_back([this] {this->work_routine(); });
		}

		for (auto& th : threads) th.join();
	}
};