#pragma once
#include "Runnable.h"
#include <mutex>
#include <atomic>
#include <vector>
#include <iostream>
class Conc_Jobs_Container {
	// vector based container for jobs to be done by threads in WorkFlow.
	// RULE: All methods are thread-safe when param lock = true (default for all methods)
	//TODO:: Implement RAII  
private:
	using Job = std::pair<Runnable*, int>;
	using Jobs = std::vector<Job>;
	using iterator = Jobs::iterator;

	int completed_till_here = 0;
	std::mutex container_mutex;
	int state = eNot_started;
	Jobs jobs;

public:
	std::atomic_bool is_completed{ false }; 
	enum States {
		eNot_Ready,
		eNot_started,
		eProcessing,
		eCompleted,
		eLatch
	};
	Conc_Jobs_Container() : container_mutex{} {}
	void inline lock_if(bool lock = true) { if (lock) container_mutex.lock(); }
	void inline unlock_if(bool lock = true) { if (lock) container_mutex.unlock(); }
	iterator inline begin(bool lock = true) {
		lock_if(lock);
		jobs.begin();
		unlock_if(lock);
	}
	iterator inline end(bool lock = true) {
		lock_if(lock);
		jobs.end();
		unlock_if(lock);
	}
	inline Job get_job(int i, bool lock = true) {
		lock_if(lock);
		Job result = jobs[i];
		unlock_if(lock);
		return result;
	}

	void add_runnable(Runnable& r, bool lock = true) {
		lock_if(lock);
		jobs.emplace_back(&r, eNot_started);
		unlock_if(lock);
	}

	void add_latch(int th_nr,bool lock = true) {
		lock_if(lock);
		Job* job_ptr = new Job{};
		Latch* latch_ptr = new Latch{th_nr,&(job_ptr->second), (*this) };
		job_ptr->first = latch_ptr;
		job_ptr->second = eLatch;
		jobs.push_back(*job_ptr);
		unlock_if(lock);
	}
	inline Jobs::iterator update_completed_index(bool lock = true) {
		lock_if(lock);
		auto iter = std::find_if<Jobs::iterator>(jobs.begin() + completed_till_here,
			jobs.end(),
			[](Job job) {return job.second = eCompleted; });
		completed_till_here = iter - jobs.begin();
		unlock_if(lock);
		return iter;
	}
	
	inline void erase_completed_(bool lock = true) {
		lock_if(lock);
		auto iter = update_completed_index(false);
		jobs.erase(jobs.begin() + completed_till_here, iter);
		completed_till_here = 0;
		unlock_if(lock);
	}

	inline Runnable* dispatch_next_job(bool lock = true) {
		//runs job, and for convenience also returns ptr
		lock_if(lock);
		update_completed_index(false);
		if (completed_till_here == jobs.size()) {
			is_completed.store(true);
		}
		else{
			Job& job = jobs[completed_till_here];
			if (job.second != eLatch){
				++completed_till_here;
				job.second = eProcessing;
			}
			else {
				std::cout << "FOUND LATCH";
			}
			unlock_if(lock);

			job.first->run();

			return job.first;
		}
		unlock_if(lock);
		return nullptr;
	}



	struct Latch :public Runnable {
		std::mutex mx;
		std::condition_variable cond;
		std::atomic_int threads_remaining;
		Conc_Jobs_Container& container;
		int*status;

	public:
		Latch(int th_nr, int* status_, Conc_Jobs_Container& container_) : status{ status_ }, container { container_ }, threads_remaining{ th_nr } {}

		virtual void job_to_run() override {
			if (threads_remaining-- > 1) {
				if constexpr (Test::ctest) std::cout << "still waiting in latch";
				std::unique_lock<std::mutex> lock(mx);
				while (!done) { // Wait inside loop to handle spurious wakeups etc.
					cond.wait(lock, [this]() {return this->threads_remaining.load() == 0; });
					if constexpr (Test::ctest) std::cout << "Thread is awake!" << std::endl;
					
				}
				if constexpr (Test::ctest) std::cout << "Thread is out of loop" << std::endl;

			}
			else {
				if (!done) {
					if constexpr (Test::ctest) std::cout << "Latch opened" << std::endl;
					*status = eCompleted;		
					container.completed_till_here++;
					done == true;
					cond.notify_all();
				}
			};
		};
	};







};


