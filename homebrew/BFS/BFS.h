#pragma once

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <deque>
#include <map>
#include <iostream>
#include <type_traits>
#include "Empty_Visitor.h"
#include "Graph_Types.h"
template <typename Graph,typename Visitor=Empty_Visitor<Graph>>
// GRAPH should be an adjacency list or similar
class BFS{
public:
	typedef typename boost::graph_traits<Graph>::vertex_iterator Vertex_iter;
	typedef typename boost::graph_traits<Graph>::edge_iterator Edge_iter;
	typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex_handle;
	typedef typename boost::graph_traits<Graph>::edge_descriptor Edge_handle;
	typedef typename boost::graph_traits<Graph>::out_edge_iterator out_edge_iter;
	enum class eColor : int
	{
		eWhite,
		eGray,
		eBlack
	};
	std::vector<Vertex_handle> int_to_handle;
	std::vector<eColor> color_vec;
	std::map<Vertex_handle, int> handle_to_int;
	std::deque<Vertex_handle> vertex_queue{};
	Visitor& visitor;
	Graph& graph;
	Vertex_handle source;

	BFS(Graph& graph_, Visitor& visitor_) : graph{ graph_ },
		source{ *boost::vertices(graph).first },
		handle_to_int{},
		visitor{ visitor_ },
		color_vec(cvertex_count, eColor::eWhite),
		int_to_handle{}
	{
		int_to_handle.reserve(cvertex_count);
		int counter{ 0 };
		for (auto iter = vertices(graph); iter.first != iter.second; iter.first++) {
			std::cout << counter;
			int_to_handle.push_back(*iter.first);
			handle_to_int[*iter.first] = counter++;
		}
		Test::print("finished constructing BFS");
	};
	BFS& set_source(const Vertex_handle u) { source = u; return this; };


	void inline set_color(const eColor c, const Vertex_handle v) {
		color_vec[handle_to_int[v]] = c;
	};

	eColor inline get_color(const Vertex_handle v){
		int index{ handle_to_int[v] };
		return color_vec[  index  ];
	};


	template<typename T>
	void print_queue(T q) { // NB: pass by value so the print uses a copy
		while (!q.empty()) {
			std::cout << q.top() << ' ';
			q.pop();
		}
		std::cout << '\n';
	}
	void start() {

		//initialization
		set_color(eColor::eGray, source);
		vertex_queue.push_back(source); 
		// starting:
		Test::print("printing queue size:", vertex_queue.size());
		while (!vertex_queue.empty()) {
			auto vert = vertex_queue.back();
			Test::print("printing queue pop:",vert);
			vertex_queue.pop_back();
			visitor.examine_vertex(vert);
			for (auto e_iter = out_edges(vert, graph); e_iter.first != e_iter.second; e_iter.first++) {
				Edge_handle ed = *e_iter.first;
				visitor.examine_edge(ed);
				Vertex_handle v = target(ed, graph);
				if (get_color(v) == eColor::eWhite) {
					visitor.discover_vertex(v);
					set_color(eColor::eGray, v);
					vertex_queue.push_back(v);
				}
			}
			visitor.finish_vertex(vert);
			set_color(eColor::eBlack, vert);
		}
	};
};