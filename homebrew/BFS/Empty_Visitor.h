#pragma once
#include <iostream>
template<typename Graph>
class Empty_Visitor {
public:
	Graph& graph;
	Empty_Visitor(Graph& graph_) : graph{ graph_ } {};
	typedef typename boost::graph_traits<Graph>::vertex_iterator Vertex_iter;
	typedef typename boost::graph_traits<Graph>::edge_iterator Edge_iter;
	typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex_handle;
	typedef typename boost::graph_traits<Graph>::edge_descriptor Edge_handle;
	typedef typename boost::graph_traits<Graph>::out_edge_iterator out_edge_iter;
	void inline examine_vertex(const Vertex_handle u) {};
	void inline discover_vertex(const Vertex_handle u) {};
	void inline examine_edge(const Edge_handle e) {};
	void inline finish_vertex(const Vertex_handle u) {};


};